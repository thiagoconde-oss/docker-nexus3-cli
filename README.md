# docker-nexus3-cli

A Docker image with the [nexus3-cli](https://gitlab.com/thiagoconde-oss/nexus3-cli/) application
pre-installed.


## Environment variables

When running the client in a docker container, you need to either:

1. mount a volume so that the `~/.nexus3-cli` configuration file is available in the container; or
2. pass the [login options](https://nexus3-cli.readthedocs.io/en/latest/cli.html#nexus3-login)
   using the variables below.

   *  `NEXUS3_LOGIN_URL`
   *  `NEXUS3_LOGIN_USERNAME`
   *  `NEXUS3_LOGIN_PASSWORD`
   *  `NEXUS3_LOGIN_X509_VERIFY` (optional)

These are only the options required for authentication but the client accepts any [command-line 
options](https://nexus3-cli.readthedocs.io/en/latest/cli.html#) as environment variables. 


## Usage

Configure your client:

```bash
docker run -it --rm -v $(pwd):/root thiagofigueiro/nexus3-cli login
```

Once you have a configuration file, use the client as you normally would:

```bash
docker run --rm --env-file ~/.nexus3-cli.env thiagofigueiro/nexus3-cli repo list 
```

If you're transferring files, you need to mount the volume (`-v`) so the container has access to the host file system.

Create an alias so you can call `nexus3` as if it was installed as a command:

```bash
alias nexus3="docker run -t --rm --env-file ~/.nexus-cli.env thiagofigueiro/nexus3-cli"
```

### Examples

Start a test Nexus service:
```bash
$ docker run -d --rm -p 8081:8081 --name nexus sonatype/nexus3
$ docker tail nexus
# ...
2020-04-26 22:10:33,319+0000 INFO  [jetty-main-1] *SYSTEM org.sonatype.nexus.bootstrap.jetty.JettyServer -
-------------------------------------------------

Started Sonatype Nexus OSS 3.22.0-02

-------------------------------------------------
```

Create client configuration (`192.168.1.7` is the ip address of my docker host):

```bash
# First, grab the password from the Nexus service
$ docker exec nexus cat /nexus-data/admin.password
a5e6c23b-d548-45aa-9d66-43e6da8e734d

# Now use it to login
$ docker run -it --rm -v $(pwd):/root thiagofigueiro/nexus3-cli login
Url [http://localhost:8081]: http://192.168.1.7:8081
Username [admin]:
Password: <paste a5e6c23b-d548-45aa-9d66-43e6da8e734d - output suppressed>
X509 verify [Y/n]: n

Login successful.
Configuration saved to /root/.nexus-cli, /root/.nexus-cli.env

# Have a look at the file generated in my home directory (thanks to the `-v $(pwd):/root volume`):
$ cat ~/.nexus-cli.env
NEXUS3_API_VERSION=v1
NEXUS3_PASSWORD=a5e6c23b-d548-45aa-9d66-43e6da8e734d
NEXUS3_URL=http://192.168.1.7:8081/
NEXUS3_USERNAME=admin
NEXUS3_X509_VERIFY=False
```

Define an alias and use it to run some commands

```bash
$ alias nexus3="docker run -t --rm --env-file ~/.nexus-cli.env thiagofigueiro/nexus3-cli"
$ nexus3 repo list
Name              Format   Type     URL
nuget-group       nuget    group    http://192.168.1.7:8081/repository/nuget-group
maven-snapshots   maven2   hosted   http://192.168.1.7:8081/repository/maven-snapshots
maven-central     maven2   proxy    http://192.168.1.7:8081/repository/maven-central
nuget.org-proxy   nuget    proxy    http://192.168.1.7:8081/repository/nuget.org-proxy
maven-releases    maven2   hosted   http://192.168.1.7:8081/repository/maven-releases
nuget-hosted      nuget    hosted   http://192.168.1.7:8081/repository/nuget-hosted
maven-public      maven2   group    http://192.168.1.7:8081/repository/maven-public

# You need to enable scripts in Nexus for some actions :(
$ nexus3 repo show maven-public
Error: b'{\n  "name" : "nexus3-cli-repository-get",\n  "result" : "Creating and updating scripts is disabled"\n}'
```

Upload all files and directories within `/some/path` to the `repo-name` Nexus 3 repository.
```bash
docker run --rm --env-file ~/.nexus3-cli.env -v /some/path:/some/path -w /some/path thiagofigueiro/nexus3-cli \
    up . repo-name/ 
```

## Build image

The usual:
```bash
docker build --pull -t nexus3-cli .
```

### Arguments

The `nexus3-cli` version installed in the docker image is controlled by the arguments below:

- `NEXUS3_CLI_GE_VERSION`: use a version greater than or equal to this number.
- `NEXUS3_CLI_LT_VERSION`: use a version less than this number

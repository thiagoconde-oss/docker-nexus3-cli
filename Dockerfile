FROM python:3-alpine
ARG NEXUS3_CLI_GE_VERSION=3
ARG NEXUS3_CLI_LT_VERSION=4
USER root
RUN apk add \
      --no-cache --update --virtual .build-deps build-base libffi-dev openssl-dev rust cargo \
    && pip3 install "nexus3-cli>=${NEXUS3_CLI_GE_VERSION},<${NEXUS3_CLI_LT_VERSION}" \
    && apk del .build-deps \
    && rm -rf ~/.cache/pip
ENTRYPOINT ["/usr/local/bin/nexus3"]
